# Melanoma Thesis Assets

This repository contains the artifacts/assets:

- Trained models
- File configurations

Of my master thesis in Data Science. The source code and
the documentation is hosted in this [repository](https://github.com/wilberquito/melanoma.thesis)
on GitHub.
